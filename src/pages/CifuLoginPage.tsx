import {
  IonButton,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import './CifuLoginPage.scss'

const CifuLoginPage: React.FC = () => {
  return (
    <IonPage className="CifuPage CifuLoginPage">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Cifu Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding ion-text-center">
        <div>logo</div>
        <div className="cifu-bg ion-padding rounded">
          <h2>
            <IonText color="primary">cifu login</IonText>
          </h2>
          <IonList>
            <IonItem>
              <IonLabel color="light">username</IonLabel>
              <IonInput color="light"></IonInput>
            </IonItem>
            <IonItem>
              <IonLabel color="light">password</IonLabel>
              <IonInput color="light"></IonInput>
            </IonItem>
          </IonList>
          <IonButton>login</IonButton>
        </div>
      </IonContent>
    </IonPage>
  )
}

export default CifuLoginPage
