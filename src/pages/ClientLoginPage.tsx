import {
  IonButton,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonPage,
  IonText,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import './ClientLoginPage.scss'

const ClientLoginPage: React.FC = () => {
  return (
    <IonPage className="ClientPage ClientLoginPage">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Client Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding ion-text-center">
        <div>logo</div>
        <div className="client-bg ion-padding rounded">
          <h2>
            <IonText color="primary">client login</IonText>
          </h2>
          <IonList>
            <IonItem>
              <IonLabel color="light">username</IonLabel>
              <IonInput color="light"></IonInput>
            </IonItem>
            <IonItem>
              <IonLabel color="light">password</IonLabel>
              <IonInput color="light"></IonInput>
            </IonItem>
          </IonList>
          <IonButton>login</IonButton>
        </div>
      </IonContent>
    </IonPage>
  )
}

export default ClientLoginPage
