import {
  IonContent,
  IonHeader,
  IonItem,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import './Home.css'

const Home: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Blank</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonItem routerLink="/login/cifu">Cifu Login</IonItem>
        <IonItem routerLink="/login/client">Client Login</IonItem>
      </IonContent>
    </IonPage>
  )
}

export default Home
